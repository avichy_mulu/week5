#include "Polygon.h"
#include <iostream>

std::string Polygon::getType() const
{
	return this->Type;
}

std::string Polygon::getName() const
{
	return this->Name;
}

Polygon::Polygon(const std::string& type, const std::string& name) : Shape(type,name)
{
}

void Polygon::printDetails() const
{
}

Polygon::~Polygon()
{}