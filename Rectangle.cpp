#include "Rectangle.h"

void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(this->a, this->a);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(this->a, this->a);
}

myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name) : Polygon(type,name), a(a)
{
	this->a = a;
	this->length = length;
	this->width = width;
}

double myShapes::Rectangle::getArea() const
{
	double area = 0;
	area = this->length * this->width;
	return area;
}

double myShapes::Rectangle::getPerimeter() const 
{
	double perimeter = 0;
	perimeter = this->length * 2 + this->width * 2;
	return perimeter;
}

void myShapes::Rectangle::move(const Point& other) 
{
	a += other;
} // add the Point to all the points of shape

myShapes::Rectangle::~Rectangle()
{
}
