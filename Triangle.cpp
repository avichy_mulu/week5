#include "Triangle.h"


void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(this->a, this->b,this->c);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(this->a, this->b, this->c);
}

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) : Polygon(type,name), a(a), b(b) ,c(c)
{
}

double Triangle::getArea() const
{
	double area = 0;
	double s = this->getPerimeter() / 2;
	area = sqrt(s * (s - this->a.distance(this->b)) * (s - this->b.distance(this->c)) * (s - this->c.distance(this->a)));
	return area;
}

double Triangle::getPerimeter() const
{
	double perimeter = 0;
	perimeter = this->a.distance(this->b) + this->b.distance(this->c) + this->c.distance(this->a);
	return perimeter;
}

void Triangle::move(const Point& other)
{
	a += other;
	b += other;
	c += other;
}// add the Point to all the points of shape

Triangle::~Triangle()
{}