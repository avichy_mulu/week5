#include "Circle.h"
#include <iostream>
#define PI 3.14159265359

void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}

Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name) : center(center),Shape(type,name)
{
	this->radius = radius;
}

const Point& Circle::getCenter() const
{
	return this->center;
}

double Circle::getRadius() const
{
	return this->radius;
}

double Circle::getArea() const
{
	double area = 0;
	area = this->getRadius() * this->getRadius() *PI;
	return area;
}

double Circle::getPerimeter() const
{
	double perimeter = 0;
	perimeter = 2 * PI* this->getRadius();
	return perimeter;
}

void Circle::move(const Point& other)
{
	center += other;
} // add the Point to all the points of shape

Circle::~Circle()
{}

