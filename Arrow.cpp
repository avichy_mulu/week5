#include "Arrow.h"

void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(this->a, this->b);
}
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(this->a, this->b);
}


Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name) : Shape(type,name),a(a),b(b)
{
}

void Arrow::move(const Point& other)
{
	this->a += other;
	this->b += other;
}

double Arrow::getArea() const
{
	return 0;
}


double Arrow::getPerimeter() const
{
	return this->a.distance(b);
}

Arrow::~Arrow()
{}