#include "Menu.h"
#include <iostream>
void main()
{
	int choice = 0, choose=0;
	std::string name = "";
	Menu menu;
	while (choice != 3)
	{
		menu.viewMenu();
		std::cin >> choice;
		if (choice==0)
		{
			menu.new_shape_menu();
			std::cin >> choice;
			menu.new_shape(choice);
		}

		else if (choice == 1)
		{
			std::string name = menu.getName();
			std::cout << "Enter 0 to move the shape\nEnter 1 to get its details.\nEnter 2 to remove the shape.";
			std::cin >> choice;
			if (!choice)
			{
				menu.move(name);
			}
			else if (choice==1)
			{
				menu.getDetails(name);
			}
			if (choice==2)
			{
				menu.clear(name);
			}
		}

		else if (choice == 2)
		{
			menu.deleteAll();
		}
		std::cout << std::endl;
	}

}