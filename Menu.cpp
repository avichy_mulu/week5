#include "Menu.h"
#include "Polygon.h"
#include "Triangle.h"
#include "Circle.h"
#include "Rectangle.h"
#include "Arrow.h"
#include <iostream>


Menu::Menu()
{
	count = 0;
	this->array_of_shapes = new Shape*;
	_canvas;
}

Menu::~Menu()
{
	delete[] this->array_of_shapes;
}

int Menu::getCount()
{
	return this->count;
}


void Menu::addToArray(Shape* other)
{
	this->array_of_shapes[count] = other;
	this->count += 1;
}


void Menu::viewMenu()
{
	std::cout << "Enter 0 to add a new shape." << std::endl;
	std::cout << "Enter 1 to modify or get information from a current shape." << std::endl;
	std::cout << "Enter 2 to delete all of the shapes." << std::endl;
	std::cout << "Enter 3 to exit." << std::endl;
}

void Menu::new_shape_menu()
{
	std::cout << "Enter 0 to add a circle." << std::endl;
	std::cout << "Enter 1 to add an arrow." << std::endl;
	std::cout << "Enter 2 to add a triangle." << std::endl;
	std::cout << "Enter 3 to add a rectangle." << std::endl;
}


void Menu::new_shape(int choice)
{
	if (!choice)
	{
		double radius = 0, x = 0, y = 0;
		std::string type = "Circle", name = "";
		std::cout << "Enter X: ";
		std::cin >> x;
		std::cout << "Enter Y: ";
		std::cin >> y;
		while (radius <= 0)
		{
			std::cout << "Enter the radius: ";
			std::cin >> radius;
		}
		std::cout << "Enter the name: ";
		std::cin >> name;
		Point center(x,y);
		Circle* circle = new Circle(center,radius,type,name);
		this->addToArray(circle);
		circle->draw(_canvas);
	}

	else if (choice==1)
	{
		std::string type = "Arrow", name = "";
		std::cout << "Enter the name: ";
		std::cin >> name;
		Arrow* arrow = new Arrow(this->get_new_point(2), this->get_new_point(1),type,name);
		this->addToArray(arrow);
		arrow->draw(_canvas);
	}


	else if (choice == 2)
	{
		std::string type = "Triangle", name = "";
		std::cout << "Enter the name: ";
		std::cin >> name;
		Triangle* triangle= new Triangle(this->get_new_point(3), this->get_new_point(2), this->get_new_point(1),type, name);
		this->addToArray(triangle);
		triangle->draw(_canvas);
	}


	else if (choice == 3)
	{
		int length = 0, width = 0;
		std::string type = "Rectangle", name = "";
		std::cout << "Enter the name: ";
		std::cin >> name;
		while (length <= 0 || width <= 0)
		{
			std::cout << "Enter the length: ";
			std::cin >> length;
			std::cout << "Enter the width: ";
			std::cin >> width;
		}
		myShapes::Rectangle* rectangle = new myShapes::Rectangle(this->get_new_point(1),length,width,type, name);
		this->addToArray(rectangle);
		rectangle->draw(_canvas);
	}
}

Point Menu::get_new_point(int num)
{
	double x = 0, y = 0;
	std::cout << "Enter X for point " << num << " : ";
	std::cin >> x;
	std::cout << "Enter Y for point " << num << " : ";
	std::cin >> y;
	Point new_point(x,y);
	return new_point;
}


void Menu::deleteAll()
{
	delete[] this->array_of_shapes;
	this->array_of_shapes = new Shape*;
}


void Menu::getDetails(std::string name)
{
	int i = 0;
	for (i = 0; i <=this->count; i++)
	{
		if (name == this->array_of_shapes[i]->getName())
		{
			this->array_of_shapes[i]->printDetails();
			i += this->count;
		}
	}
}


void Menu::move(std::string name)
{
	int i = 0;
	for (i = 0; i <=this->count; i++)
	{
		if (name == this->array_of_shapes[i]->getName())
		{
			this->array_of_shapes[i]->clearDraw(_canvas);
			this->array_of_shapes[i]->move(this->get_new_point(1));
			this->array_of_shapes[i]->draw(_canvas);
			i += this->count;
		}
	}
}


void Menu::clear(std::string name)
{
	int i = 0;
	for (i = 0; i <=this->count; i++)
	{
		if (name == this->array_of_shapes[i]->getName())
		{
			this->array_of_shapes[i]->clearDraw(_canvas);
			i += this->count;
		}
	}
}


std::string Menu::getName()
{
	int i = 0,choice=0;
	for (i = 0; i <=this->count; i++)
	{
		std::cout << "Enter " << i << "for " << this->array_of_shapes[i]->getName();
		std::cout << std::endl;
		i += this->count;
	}
	std::cin >> choice;
	return this->array_of_shapes[choice]->getName();
}