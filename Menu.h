#pragma once
#include "Shape.h"
#include "Canvas.h"
#include <vector>



class Menu
{
public:
	Menu();
	~Menu();
	int getCount();
	// more functions..
	void addToArray(Shape* other);
	void viewMenu();
	void new_shape_menu();
	void new_shape(int choice);
	Point get_new_point(int num);
	void deleteAll();
	void getDetails(std::string name);
	void move(std::string name);
	void clear(std::string name);
	std::string getName();
private: 
	Canvas _canvas;
	int count;
	Shape** array_of_shapes;
};

