#include "Shape.h"
#include <iostream>


void Shape::printDetails() const
{
	std::cout << "Area is: " << this->getArea();
	std::cout << std::endl;
	std::cout << "Perimeter is: " << this->getPerimeter();
	std::cout << std::endl;
	std::cout << "Type is: " << this->getType();
	std::cout << std::endl;
	std::cout << "Name is: " << this->getName();
	std::cout << std::endl;
}

std::string Shape::getType() const
{
	return this->name;
}

std::string Shape::getName() const
{
	return this->type;
}


Shape::Shape(const std::string& name, const std::string& type)
{
	this->name = name;
	this->type = type;
}