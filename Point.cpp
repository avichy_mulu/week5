#include "Point.h"
#include <Math.h>


Point Point::operator+(const Point& other) const
{
	Point new_point(this->x + other.getX(),this->y + other.getY() );
	return new_point;
}


Point& Point::operator+=(const Point& other) 
{
	this->x += other.getX();
	this->y += other.getY();
	return *this;
}

Point::Point (double x, double y)
{
	this->x = x;
	this->y = y;
}



Point::Point(const Point& other)
{
	this->x = other.getX();
	this->y = other.getY();
}



double Point::getX() const
{
	return this->x;
}


double Point::getY() const 
{
	return this->y;
}

double Point::distance(const Point& other) const 
{
	double distance = 0;
	distance = sqrt(pow(this->getX() -other.getX(),2) + pow(this->getY() - other.getY(), 2));
	return distance;
}


Point::~Point()
{}