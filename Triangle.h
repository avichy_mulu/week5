#include "Polygon.h"
#include <string>

class Triangle : public Polygon
{
private:
	Point a;
	Point b;
	Point c;
public:
	Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name);
	virtual ~Triangle();
	void virtual draw(const Canvas& canvas);
	void virtual clearDraw(const Canvas& canvas);
	// override functions if need (virtual + pure virtual)
	virtual double getArea() const;
	virtual double getPerimeter() const;
	virtual void move(const Point& other) ; // add the Point to all the points of shape
};